/*
 * GoWinds API Client
 *
 *  Copyright 2023 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package client

import "fmt"

func (c *Client) FetchHosts() ([]Host, error) {
	return c.FetchHostsOf(c.hash)
}

func (c *Client) FetchHostsOf(hash string) ([]Host, error) {
	r, err := c.getRequest(fmt.Sprintf("accounts/%s/hosts", hash))
	if err != nil {
		return nil, fmt.Errorf("unable to create fetch hosts request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return nil, fmt.Errorf("unable to auth fetch hosts request: %w", err)
	}
	var lhr ListHostsResponse
	err = c.doWithArgs(r, map[string]string{
		"recursive": "false",
	}, &lhr)
	if err != nil {
		return nil, fmt.Errorf("unable to fetch hosts request: %w", err)
	}
	return lhr.Hosts, nil
}

func (c *Client) FetchOrigins() ([]Origin, error) {
	return c.FetchOriginsOf(c.hash)
}

func (c *Client) FetchOriginsOf(hash string) ([]Origin, error) {
	r, err := c.getRequest(fmt.Sprintf("accounts/%s/origins", hash))
	if err != nil {
		return nil, fmt.Errorf("unable to create fetch origins request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return nil, fmt.Errorf("unable to auth fetch origins request: %w", err)
	}
	var lor ListOriginsResponse
	err = c.doBareRequest(r, &lor)
	if err != nil {
		return nil, fmt.Errorf("unable to fetch origins request: %w", err)
	}
	return lor.Origins, nil
}

func (c *Client) FetchRawConfiguration(acct, host string, scope int) (map[string]any, error) {
	r, err := c.getRequest(fmt.Sprintf("accounts/%s/hosts/%s/configuration/%d", acct, host, scope))
	if err != nil {
		return nil, fmt.Errorf("unable to create fetch raw configuration request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return nil, fmt.Errorf("unable to auth fetch raw configuration request: %w", err)
	}
	var rc map[string]any
	err = c.doBareRequest(r, &rc)
	if err != nil {
		return nil, fmt.Errorf("unable to fetch raw configuration request: %w", err)
	}
	return rc, nil
}

func (c *Client) FetchRawConfigurationV4(acct, host string, scope int) (map[string]any, error) {
	r, err := c.getRequest(fmt.Sprintf("cdn/accounts/%s/sites/%s/configuration/%d", acct, host, scope), WithV4())
	if err != nil {
		return nil, fmt.Errorf("unable to create fetch raw configuration request v4: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return nil, fmt.Errorf("unable to auth fetch raw configuration request v4: %w", err)
	}
	var rc map[string]any
	err = c.doBareRequest(r, &rc)
	if err != nil {
		return nil, fmt.Errorf("unable to fetch raw configuration request v4: %w", err)
	}
	return rc, nil
}

func (c *Client) PutRawConfigurationV4(acct, host string, scope int, body []byte) (map[string]any, error) {
	r, err := c.putRequest(fmt.Sprintf("cdn/accounts/%s/sites/%s/configuration/%d", acct, host, scope), WithV4())
	if err != nil {
		return nil, fmt.Errorf("unable to create put raw configuration request v4: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return nil, fmt.Errorf("unable to auth put raw configuration request v4: %w", err)
	}
	var rc map[string]any
	err = c.do(r, WithBody(body), WithTarget(&rc))
	if err != nil {
		return nil, fmt.Errorf("unable to put raw configuration request v4: %w", err)
	}
	return rc, nil
}

func (c *Client) CheckConfigUpdateStatus(acct, host string, scope int, job string) (float64, error) {
	r, err := c.getRequest(fmt.Sprintf("cdn/accounts/%s/sites/%s/configuration/%d/%s", acct, host, scope, job), WithV2())
	if err != nil {
		return 0, fmt.Errorf("unable to create check config update status request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return 0, fmt.Errorf("unable to auth check config update status request: %w", err)
	}
	var rc map[string]any
	err = c.doBareRequest(r, &rc)
	if err != nil {
		return 0, fmt.Errorf("unable to check config update status request: %w", err)
	}
	if _, ok := rc["progress"]; !ok {
		return 0, fmt.Errorf("unable to check config update status request: no progress field")
	}
	return rc["progress"].(float64), nil
}
