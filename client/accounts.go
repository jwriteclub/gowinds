/*
 * GoWinds API Client
 *
 *  Copyright 2023 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package client

import "fmt"

func (c *Client) FetchAccount(hash string) (Account, error) {
	r, err := c.getRequest(fmt.Sprintf("accounts/%s", hash))
	if err != nil {
		return Account{}, fmt.Errorf("unable to create fetch account request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return Account{}, fmt.Errorf("unable to auth fetch account request: %w", err)
	}
	var acct Account
	err = c.doBareRequest(r, &acct)
	if err != nil {
		return Account{}, fmt.Errorf("unable to fetch account request: %w", err)
	}
	acct.HashCode = hash // Psych! If you fetch it by the hash code it doesn't populate in the response.
	return acct, nil
}

func (c *Client) FetchCurrentAccount() (Account, error) {
	return c.FetchAccount(c.hash)
}

func (c *Client) FetchSubAccounts() ([]Account, error) {
	return c.FetchSubAccountsOf(c.hash)
}

func (c *Client) FetchSubAccountsOf(hash string) ([]Account, error) {
	r, err := c.getRequest(fmt.Sprintf("accounts/%s/subaccounts", hash))
	if err != nil {
		return nil, fmt.Errorf("unable to create fetch subaccounts request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return nil, fmt.Errorf("unable to auth fetch subaccounts request: %w", err)
	}
	var ma Account
	err = c.doWithArgs(r, map[string]string{
		"recursive": "true",
	}, &ma)
	if err != nil {
		return nil, fmt.Errorf("unable to fetch subaccounts request: %w", err)
	}
	return ma.SubAccounts, nil
}
