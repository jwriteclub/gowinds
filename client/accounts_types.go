/*
 * GoWinds API Client
 *
 *  Copyright 2023 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package client

type Account struct {
	Name                      string         `json:"accountName"`
	Status                    string         `json:"accountStatus"`
	BillingAccountID          string         `json:"billingAccountId"`
	BillingAccountNumber      string         `json:"billingAccountNumber"`
	BillingContact            BillingContact `json:"billingContact"`
	GCSLogsBucket             string         `json:"gcsLogsBucket"`
	HashCode                  string         `json:"accountHash"` // Default account identifier
	ParentHash                string         `json:"parentHash"`
	ID                        int            `json:"id"`
	MaxHCSTenants             int            `json:"maxHcsTenants"`
	MaxDirectSubAccounts      int            `json:"maximumDirectSubAccounts"`
	MaxHosts                  int            `json:"maximumHosts"`
	NOCContact                Contact        `json:"nocContact"`
	Parent                    int            `json:"parent"`
	PrimaryContact            Contact        `json:"primaryContact"`
	Services                  []Service      `json:"services"`
	SubAccountCreationEnabled bool           `json:"subAccountCreationEnabled"`
	SubAccounts               []Account      `json:"subAccounts"`
	SupportEmail              string         `json:"supportEmailAddress"`
	TechnicalContact          Contact        `json:"technicalContact"`
}

type Service struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Type        string `json:"type"`
}

type Contact struct {
	ID    int    `json:"id"`
	First string `json:"firstName"`
	Last  string `json:"lastName"`
	Email string `json:"email"`
	Phone string `json:"phone"`
	Fax   string `json:"fax"`
}

type BillingContact struct {
	Contact
	BillingAccountNumber string `json:"billingAccountNumber"`
}
