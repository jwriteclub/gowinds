/*
 * GoWinds API Client
 *
 *  Copyright 2020 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package client

import (
	"bitbucket.org/jwriteclub/gowinds/config"
	"fmt"
	"github.com/tenta-browser/polychromatic"
	"net/http"
	"time"
)

type Client struct {
	creds *config.Credentials
	h     *http.Client
	hash string
}

func DefaultClient() (*Client, error) {
	creds, err := config.GetCredentials()
	if err != nil {
		return nil, fmt.Errorf("unable to create default config: %w", err)
	}
	return NewClient(creds)
}

func NewClient(credentials *config.Credentials) (*Client, error) {
	ret := &Client{creds: credentials, h: &http.Client{
		Transport: &http.Transport{
			ForceAttemptHTTP2:   true,
			MaxIdleConns:        10,
			MaxIdleConnsPerHost: 10,
			IdleConnTimeout:     60 * time.Second,
		},
		Timeout: 60 * time.Second,
	}}
	r, err := ret.getRequest("users/me")
	if err != nil {
		return nil, fmt.Errorf("unable to create request to determine current user: %w", err)
	}
	err = ret.addAuth(r)
	if err != nil {
		return nil, fmt.Errorf("unable to add auth to determine current user: %w", err)
	}
	var mr meResponse
	err = ret.doBareRequest(r, &mr)
	if err != nil {
		return nil, fmt.Errorf("unable to determine current user request: %w", err)
	}
	if !mr.IsActive() {
		return nil, fmt.Errorf("unable to use current account with status %s", mr.Status)
	}
	ret.hash = mr.Hash
	polychromatic.GetLogger("gowinds-api-client-initializer").Tracef("configured with account hash %s", ret.hash)
	return ret, nil
}

