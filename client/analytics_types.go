/*
 * GoWinds API Client
 *
 *  Copyright 2020 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package client

type analyticsApiResponse struct {
	Series []analyticsSeries `json:"series"`
}

type analyticsSeries struct {
	Type string `json:"type"`
	Key string `json:"key"`
	Metrics []string `json:"metrics"`
	Data [][]float64 `json:"data"`
}

type AnalyticsEntry struct {
	Transfer float64 `hwnd:"xferUsedTotalMB"`
	Rate float64 `hwnd:"ninetyFiveFiveMbps"`
	Requests int `hwnd:"requestsCountTotal"`
}

type AnalyticsList struct {
	Type string
	Key string
	Data []AnalyticsEntry
}