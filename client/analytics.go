/*
 * GoWinds API Client
 *
 *  Copyright 2020 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package client

import (
	"fmt"
	"reflect"
	"time"
)

func (c *Client) FetchUsageByAccount(from, to time.Time, gran Granularity, plat Platform) ([]AnalyticsList, error){
	r, err := c.getRequest(fmt.Sprintf("accounts/%s/analytics/transfer", c.hash))
	if err != nil {
		return nil, fmt.Errorf("unable to create fetch usage request: %w", err)
	}
	err = c.addAuth(r)
	if err != nil {
		return nil, fmt.Errorf("unable to auth fetch usage request: %w", err)
	}
	var ar analyticsApiResponse
	err = c.doWithArgs(r, map[string]string{
		"startDate": formatHwndTime(from),
		"endDate": formatHwndTime(to),
		"granularity": string(gran),
		"platforms": plat.Code(),
		"groupBy": "ACCOUNT",
	}, &ar)
	if err != nil {
		return nil, fmt.Errorf("unable to fetch usage reques: %w", err)
	}
	return parseAnalyticsApiResponse(ar)
}

func parseAnalyticsApiResponse(ar analyticsApiResponse) ([]AnalyticsList, error) {
	ret := make([]AnalyticsList, 0, len(ar.Series))

	// Yes, we really have to loop these manually with integers, because
	// we need to ensure visit order, since the API response has order
	// dependencies
	for i := 0; i < len(ar.Series); i += 1 {
		s := ar.Series[i]
		item := AnalyticsList{
			Type: s.Type,
			Key: s.Key,
			Data: make([]AnalyticsEntry, 0, len(s.Data)),
		}
		if len(s.Data) < 1 {
			// TODO: How to handle empty items?
			continue
		}
		km := make(map[string]int)
		for j := 0; j < len(s.Metrics); j += 1 {
			km[s.Metrics[j]] = j
		}
		for j := 0; j < len(s.Data); j += 1 {
			d := s.Data[j]
			var e AnalyticsEntry
			t := reflect.TypeOf(e)
			for f := 0; f < t.NumField(); f += 1 {
				tag, ok := t.Field(f).Tag.Lookup("hwnd")
				if !ok {
					continue
				}
				if t.Field(f).Type.Kind() == reflect.Int {
					reflect.ValueOf(&e).Elem().Field(f).SetInt(int64(d[km[tag]]))
				} else if t.Field(f).Type.Kind() == reflect.Float64 || t.Field(f).Type.Kind() == reflect.Float32 {
					reflect.ValueOf(&e).Elem().Field(f).SetFloat(d[km[tag]])
				} else {
					return nil, fmt.Errorf("cannot parse analytics to a type of %s", t.Field(f).Type.Kind())
				}
			}
			item.Data = append(item.Data, e)
		}
		ret = append(ret, item)
	}
	return ret, nil
}