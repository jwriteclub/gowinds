/*
 * GoWinds API Client
 *
 *  Copyright 2023 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/tenta-browser/polychromatic"
	"io"
	"net/http"
	"net/url"
	"time"
)

type daType int

const (
	doTarget daType = iota
	doQuery
	doBody
)

type DoArgs interface {
	t() daType
	v() any
}

type doTargetArg struct {
	target interface{}
}

func (d doTargetArg) t() daType {
	return doTarget
}

func (d doTargetArg) v() any {
	return d.target
}

func WithTarget(target interface{}) DoArgs {
	return doTargetArg{target: target}
}

type doQueryArg struct {
	query map[string]string
}

func (d doQueryArg) t() daType {
	return doQuery
}

func (d doQueryArg) v() any {
	return d.query
}

func WithQueryArgs(args map[string]string) DoArgs {
	return doQueryArg{query: args}
}

type doBodyArg struct {
	body []byte
}

func (d doBodyArg) t() daType {
	return doBody
}

func (d doBodyArg) v() any {
	return d.body
}

func WithBody(body []byte) DoArgs {
	return doBodyArg{body: body}
}

func (c *Client) do(r *http.Request, args ...DoArgs) error {
	lg := polychromatic.GetLogger("gowinds-api-request").WithField("url", r.URL)
	var target *doTargetArg
	var query *doQueryArg
	var body *doBodyArg
	for _, arg := range args {
		switch arg.t() {
		case doTarget:
			if target != nil {
				return fmt.Errorf("duplicate target argument")
			}
			lg = lg.WithField("target", "true")
			t := arg.(doTargetArg)
			target = &t
		case doQuery:
			if query != nil {
				return fmt.Errorf("duplicate query argument")
			}
			q := arg.(doQueryArg)
			query = &q
		case doBody:
			if body != nil {
				return fmt.Errorf("duplicate body argument")
			}
			b := arg.(doBodyArg)
			body = &b
		}
	}
	lg.Debugf("Making a %s request", r.Method)
	if query != nil {
		vals := &url.Values{}
		for k, v := range (*query).v().(map[string]string) {
			vals.Add(k, v)
		}
		r.URL.RawQuery = vals.Encode()
		lg.Trace(r.URL.RawQuery)
	}
	if body != nil {
		rb := (*body).v().([]byte)
		r.Body = io.NopCloser(bytes.NewReader(rb))
		r.Header.Set("Content-Type", "application/json")
		r.Header.Set("Content-Length", fmt.Sprintf("%d", len(rb)))
		if len(rb) < 1025 {
			var pretty bytes.Buffer
			pretty.WriteByte('\n')
			err := json.Indent(&pretty, rb, "", "  ")
			pretty.WriteByte('\n')
			if err == nil {
				lg.WithField("body", "true").Trace(pretty.String())
			}
		} else {
			lg.WithField("body", "true").Tracef("<snipped %d bytes of JSON>", len(rb))
		}
	}
	for k, v := range r.Header {
		lg.Tracef(" >: %s: %s", k, v)
	}
	resp, err := c.h.Do(r)
	if err != nil {
		return fmt.Errorf("call error: %w", err)
	}
	if resp.StatusCode != 200 && resp.StatusCode != 204 {
		data, err := io.ReadAll(resp.Body) // We need to force read the request
		if err != nil {
			return fmt.Errorf("read error checking body: call failed '%d': %w", resp.Status, err)
		}
		lg.Warnf("%s", data)
		return fmt.Errorf("call failed '%s': %w", resp.Status, StatusCodeError(resp.StatusCode))
	}
	if target != nil {
		data, err := io.ReadAll(resp.Body) // We need to force read the request
		if err != nil {
			return fmt.Errorf("read error: %w", err)
		}
		if len(data) < 1025 {
			var pretty bytes.Buffer
			pretty.WriteByte('\n')
			err = json.Indent(&pretty, data, "", "  ")
			pretty.WriteByte('\n')
			if err == nil {
				lg.Trace(pretty.String())
			}
		} else {
			lg.Tracef("<snipped %d bytes of JSON>", len(data))
		}
		err = json.Unmarshal(data, (*target).v().(interface{}))
		if err != nil {
			return fmt.Errorf("unmarshal error: %w", err)
		}
	}
	return nil
}

// @deprecated - use do instead
func (c *Client) doBareRequest(r *http.Request, target interface{}) error {
	return c.do(r, WithTarget(target))
	//lg := polychromatic.GetLogger("gowinds-api-bare-request").WithField("url", r.URL)
	//lg.Debugf("Making a %s request", r.Method)
	//for k, v := range r.Header {
	//	lg.Tracef(" >: %s: %s", k, v)
	//}
	//resp, err := c.h.Do(r)
	//if err != nil {
	//	return fmt.Errorf("call error: %w", err)
	//}
	//if resp.StatusCode != 200 && resp.StatusCode != 204 {
	//	return fmt.Errorf("call failed '%s': %w", resp.Status, StatusCodeError(resp.StatusCode))
	//}
	//data, err := ioutil.ReadAll(resp.Body) // We need to force read the request
	//if err != nil {
	//	return fmt.Errorf("read error: %w", err)
	//}
	//if len(data) < 1025 {
	//	var pretty bytes.Buffer
	//	pretty.WriteByte('\n')
	//	err = json.Indent(&pretty, data, "", "  ")
	//	pretty.WriteByte('\n')
	//	if err == nil {
	//		lg.Trace(pretty.String())
	//	}
	//} else {
	//	lg.Tracef("<snipped %d bytes of JSON>", len(data))
	//}
	//err = json.Unmarshal(data, &target)
	//if err != nil {
	//	return fmt.Errorf("unmarshall error: %w", err)
	//}
	//return nil
}

// @deprecated - use do instead
func (c *Client) doWithArgs(r *http.Request, args map[string]string, target interface{}) error {
	return c.do(r, WithQueryArgs(args), WithTarget(target))
	//lg := polychromatic.GetLogger("gowinds-api-arg-request").WithField("url", r.URL)
	//lg.Debugf("Making a %s request", r.Method)
	//vals := &url.Values{}
	//for k, v := range args {
	//	vals.Add(k, v)
	//}
	//r.URL.RawQuery = vals.Encode()
	//lg.Trace(r.URL.RawQuery)
	//for k, v := range r.Header {
	//	lg.Tracef(" >: %s: %s", k, v)
	//}
	//resp, err := c.h.Do(r)
	//if err != nil {
	//	return fmt.Errorf("call error: %w", err)
	//}
	//if resp.StatusCode != 200 {
	//	data, _ := ioutil.ReadAll(resp.Body)
	//	lg.Debugf(string(data))
	//	return fmt.Errorf("call failed '%s': %w", resp.Status, StatusCodeError(resp.StatusCode))
	//}
	//lg.Debugf("Got response %s [%d]", resp.Status, resp.StatusCode)
	//for k, v := range resp.Header {
	//	lg.Tracef(" <: %s: %s", k, v)
	//}
	//data, err := ioutil.ReadAll(resp.Body)
	//if err != nil {
	//	return fmt.Errorf("read error: %w", err)
	//}
	//if len(data) < 1025 {
	//	var pretty bytes.Buffer
	//	pretty.WriteByte('\n')
	//	err = json.Indent(&pretty, data, "", "  ")
	//	pretty.WriteByte('\n')
	//	if err == nil {
	//		lg.Trace(pretty.String())
	//	}
	//} else {
	//	lg.Tracef("<snipped %d bytes of JSON>", len(data))
	//}
	//err = json.Unmarshal(data, &target)
	//if err != nil {
	//	return fmt.Errorf("unmarshall error: %w", err)
	//}
	//return nil
}

type raType int

const (
	raTypeVersion raType = iota
	raTypeHeader
)

type RequestArgs interface {
	t() raType
	v() any
}

type versionArg struct {
	ver string
}

func (v versionArg) t() raType {
	return raTypeVersion
}

func (v versionArg) v() any {
	return v.ver
}

func WithVersion(v string) RequestArgs {
	return &versionArg{v}
}

func WithV2() RequestArgs {
	return &versionArg{"v2"}
}

func WithV3() RequestArgs {
	return &versionArg{"v3"}
}

func WithV4() RequestArgs {
	return &versionArg{"v4"}
}

type headerArg struct {
	key   string
	value string
}

func (h headerArg) t() raType {
	return raTypeHeader
}

func (h headerArg) v() any {
	return [2]string{h.key, h.value}
}

func WithHeader(key, value string) RequestArgs {
	return &headerArg{key, value}
}

func (c *Client) deleteRequest(path string, args ...RequestArgs) (*http.Request, error) {
	return c.createRequest("DELETE", path, args...)
}

func (c *Client) postRequest(path string, args ...RequestArgs) (*http.Request, error) {
	return c.createRequest("POST", path, args...)
}

func (c *Client) putRequest(path string, args ...RequestArgs) (*http.Request, error) {
	return c.createRequest("PUT", path, args...)
}

func (c *Client) getRequest(path string, args ...RequestArgs) (*http.Request, error) {
	return c.createRequest("GET", path, args...)
}

func (c *Client) createRequest(method string, path string, args ...RequestArgs) (*http.Request, error) {
	ver := "v1"
	for _, arg := range args {
		switch arg.t() {
		case raTypeVersion:
			ver = arg.v().(string)
		}
	}
	req, err := http.NewRequest(method, fmt.Sprintf("https://striketracker3.highwinds.com/api/%s/%s", ver, path), nil)
	if err != nil {
		return nil, fmt.Errorf("could not create %s request to %s: %w", method, path, err)
	}
	req.Header.Add("User-Agent", "GoWinds Client/1.0")
	req.Header.Add("X-More-Info", "jwriteclub@gmail.com")
	req.Header.Add("Accept", "application/json")
	for _, arg := range args {
		switch arg.t() {
		case raTypeHeader:
			pair := arg.v().([2]string)
			req.Header.Add(pair[0], pair[1])
		}
	}
	return req, nil
}

func (c *Client) addAuth(r *http.Request) error {
	r.Header.Add("Authorization", fmt.Sprintf("Bearer %s", c.creds.ClientSecret))
	return nil
}

func formatHwndTime(t time.Time) string {
	return t.Format("2006-01-02T15:04:05Z")
}
