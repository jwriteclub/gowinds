/*
 * GoWinds API Client
 *
 *  Copyright 2023 Christopher O'Connell
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package client

type ListHostsResponse struct {
	Hosts []Host `json:"list"`
}

type Host struct {
	Name        string    `json:"name"`
	HashCode    string    `json:"hashCode"` // Primary key
	Type        string    `json:"type"`
	CreatedDate string    `json:"createdDate"`
	UpdatedDate string    `json:"updatedDate"`
	Services    []Service `json:"services"`
	Scopes      []Scope   `json:"scopes"`
}

type Scope struct {
	ID          int    `json:"id"`
	Platform    string `json:"platform"`
	Path        string `json:"path"`
	CreatedDate string `json:"createdDate"`
	UpdatedDate string `json:"updatedDate"`
}

type ListOriginsResponse struct {
	Origins []Origin `json:"list"`
}

type Origin struct {
	ID                 int    `json:"id"`
	Name               string `json:"name"`
	Type               string `json:"type"`
	Path               string `json:"path"`
	CreatedDate        string `json:"createdDate"`
	UpdatedDate        string `json:"updatedDate"`
	RequestTimeout     int    `json:"requestTimeoutSeconds"`
	ErrorCacheTTL      int    `json:"errorCacheTTLSeconds"`
	MaxRetryCount      int    `json:"maxRetryCount"`
	AuthenticationType string `json:"authenticationType"`
	Hostname           string `json:"hostname"`
	Port               int    `json:"port"`
	SecurePort         int    `json:"securePort"`
	OriginPullHeaders  string `json:"originPullHeaders"`
	OriginCacheHeaders string `json:"originCacheHeaders"`
	VerifyCertificate  bool   `json:"verifyCertificate"`
	CertificateCN      string `json:"certificateCN"`
}
